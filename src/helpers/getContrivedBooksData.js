import moment from 'moment'
import nameGenerator from 'video-game-name-generator'
import randomFullName from 'random-fullName'
import bookeGenres from '../fixtures/bookGenres'

const contrivedBook = function(name, author, genre, publishDate) {
  return {name, author, genre, publishDate}
}

function generateBookName() {
  return nameGenerator.random()
}

function generateAuthor() {
  const gender = Math.random() > 0.5 ? 'male' : 'female';
  return {
    fullName: randomFullName({gender}),
    gender,
  }
}

function generateBookGenre() {
  return bookeGenres[Math.floor(Math.random()*bookeGenres.length)]
}

function generatePublishDate(start, end) {
    return moment(start.getTime() + Math.random() * (end.getTime() - start.getTime())).format('MM.DD.YYYY')
}

export default function getContrivedBooksData(numberOfBooks) {
  const contivedBookList = []

  for (let i = 0; i < numberOfBooks; i++) {
    const name = generateBookName()
    const author = generateAuthor()
    const genre = generateBookGenre()
    const publishDate = generatePublishDate(new Date(2012, 0, 1), new Date())
    contivedBookList.push(contrivedBook(name, author, genre, publishDate))
  }

  return contivedBookList
}
