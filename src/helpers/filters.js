import moment from 'moment'

export function getFilter(o, filterBy, filterValue) {
  switch (filterBy) {
    case 'genre':
      return o.genre === filterValue
    case 'gender':
      return o.author.gender === filterValue
    default:
  }
}

export function getIndicationIndexes(indicateGenre, indicateDate, filteredList) {
  const indexes = [];

  filteredList.forEach((o, index) => {
    if (o.genre === indicateGenre) {
      if (indicateDate === 'halloween') {
        if (
          moment(o.publishDate, 'MM.DD.YYYY').date() === 31 &&
          moment(o.publishDate, 'MM.DD.YYYY').month() === 9
        ) {
          indexes.push(index);
        }
      } else if (indicateDate === 'friday') {
        if (
          moment(o.publishDate, 'MM.DD.YYYY').weekday() === 5 &&
          moment(o.publishDate, 'MM.DD.YYYY').month() !== moment(o.publishDate, 'MM.DD.YYYY').add(7, 'days').month()
        ) {
          indexes.push(index);
        }
      }
    }

  })
  console.log(indexes);

  return indexes;
}
