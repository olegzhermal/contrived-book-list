import React, { Component } from 'react'
import 'react-virtualized/styles.css'
import _ from 'lodash'
import BigTable from './BigTable'
import Filters from './Filters'
import getContrivedBooksData from './helpers/getContrivedBooksData'
import { getFilter, getIndicationIndexes } from './helpers/filters'

const booksData = getContrivedBooksData(300000)

class App extends Component {
  state = {
    sortBy: 'name',
    filterBy: null,
    filterOptions: null,
    indicateGenre: null,
    indicateDate: null,
    sortDirection: 'ASC',
    sortedList: _.orderBy(booksData, 'name', 'ASC'),
  }

  render() {
    const {
      sortBy,
      sortDirection,
      filterBy,
      filterOptions,
      indicateGenre,
      indicateDate,
      sortedList,
    } = this.state

    const filteredList = filterBy && filterOptions ?
      sortedList.filter(o => getFilter(o, filterBy.value, filterOptions.value)) : sortedList

    const indications = indicateGenre && indicateDate ?
      getIndicationIndexes(indicateGenre.value, indicateDate.value, filteredList) : null

    return (
      <div>
        <Filters
          onChange={this._onSelectChange}
          {...{filterBy, filterOptions, indicateGenre, indicateDate}}
        />
        <BigTable sort={this._sort} {...{ indications, filteredList, sortBy, filterOptions, sortDirection }}/>
      </div>
    );
  }

  _sort = ({ sortBy, sortDirection }) => {
    const sortFiled = sortBy === 'author' ? 'author.fullName' : sortBy
    const sortedList = _.orderBy(this.state.sortedList, sortFiled, sortDirection.toLowerCase())
    this.setState({sortedList, sortBy, sortDirection })
  }

  _onSelectChange = selectName => o => {
    this.setState({[selectName]: o})
  }
}

export default App;
