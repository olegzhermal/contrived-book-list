import React, { Component } from 'react'
import Select from 'react-select'
import { filterByOptions, genreOptions, genderOptions, timeOptions } from './fixtures/filters'
import 'react-select/dist/react-select.css'
import './Filters.css'

class Filters extends Component {
  render() {
    const {
      onChange,
      filterBy,
      filterOptions,
      indicateGenre,
      indicateDate,
    } = this.props

    return (
      <div className="filters_block">
        <div>
          Filter by
          <Select
            name="form-field-name"
            value={filterBy ? filterBy.value : null}
            options={filterByOptions}
            onChange={onChange('filterBy')}
          />
          {
            filterBy &&
            <Select
            name="form-field-name"
            value={filterOptions ? filterOptions.value : null}
            options={filterBy.value === 'genre' ? genreOptions : genderOptions}
            onChange={onChange('filterOptions')}
            />
          }
        </div>
        <div>
          Indicate books in the
          <Select
            name="form-field-name"
            value={indicateGenre ? indicateGenre.value : null}
            options={genreOptions}
            onChange={onChange('indicateGenre')}
          />
          <span>genre published on</span>
          <Select
            name="form-field-name"
            value={indicateDate ? indicateDate.value : null}
            options={indicateGenre ? timeOptions : null}
            onChange={onChange('indicateDate')}
          />
        </div>
      </div>
    );
  }
}

export default Filters;
