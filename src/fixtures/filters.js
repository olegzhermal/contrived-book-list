import bookGenres from './bookGenres'

export const filterByOptions = [
  { value: 'genre', label: 'Book Genre' },
  { value: 'gender', label: 'Author Gender' }
]

export const genreOptions = bookGenres.map(genre => ({value: genre, label: genre}))

export const genderOptions = [
  { value: 'male', label: 'male' },
  { value: 'female', label: 'female' }
]

export const timeOptions = [
  { value: 'halloween', label: 'Halloween' },
  { value: 'friday', label: 'last Friday of any month' }
];
