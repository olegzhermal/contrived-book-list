export default [
  {
    Header: 'Name',
    accessor: 'name'
  },
  {
    Header: 'Author',
    accessor: 'author',
  },
  {
    Header: 'Genre',
    accessor: 'genre',
  },
  {
    Header: 'Publish Date',
    accessor: 'publishDate'
  }
]
