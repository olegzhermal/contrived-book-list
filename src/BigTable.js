import React from 'react'
import { Table, Column } from 'react-virtualized'

const rowStyle = (indexesArray) => ({index}) => {
  if (indexesArray && indexesArray.includes(index)) return {background: '#a9fcac'}
}

export default function BigTable({
    sort,
    indications,
    filteredList,
    sortBy,
    sortDirection,
  }) {
    return (
      <Table
        width={1600}
        height={800}
        headerHeight={20}
        rowHeight={30}
        rowStyle={rowStyle(indications)}
        rowCount={filteredList.length}
        rowGetter={({ index }) => filteredList[index]}
        sortBy={sortBy}
        sortDirection={sortDirection}
        sort={sort}
      >
        <Column
          label='Name'
          dataKey='name'
          width={400}
        />
        <Column
          label='Author'
          dataKey='author'
          cellRenderer={o => o.rowData.author.fullName}
          width={400}
        />
        <Column
          label='Genre'
          dataKey='genre'
          width={400}
        />
        <Column
          label='Publish Date'
          dataKey='publishDate'
          width={400}
        />
      </Table>
    )
}
